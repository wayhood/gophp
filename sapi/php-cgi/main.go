package main

import (
	"log"
	"net/http/cgi"
	"os"

	"gitea.com/wayhood/gophp/core/phpctx"
	_ "gitea.com/wayhood/gophp/ext/ctype"
	_ "gitea.com/wayhood/gophp/ext/date"
	_ "gitea.com/wayhood/gophp/ext/gmp"
	_ "gitea.com/wayhood/gophp/ext/hash"
	_ "gitea.com/wayhood/gophp/ext/json"
	_ "gitea.com/wayhood/gophp/ext/pcre"
	_ "gitea.com/wayhood/gophp/ext/standard"
)

func main() {
	p := phpctx.NewProcess("cgi")
	p.CommandLine(os.Args)
	err := cgi.Serve(p.Handler("."))
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
