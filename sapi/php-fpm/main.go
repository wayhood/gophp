package main

import (
	"log"
	"net"
	"net/http/fcgi"
	"os"

	"gitea.com/wayhood/gophp/core/phpctx"
	_ "gitea.com/wayhood/gophp/ext/ctype"
	_ "gitea.com/wayhood/gophp/ext/date"
	_ "gitea.com/wayhood/gophp/ext/gmp"
	_ "gitea.com/wayhood/gophp/ext/hash"
	_ "gitea.com/wayhood/gophp/ext/json"
	_ "gitea.com/wayhood/gophp/ext/pcre"
	_ "gitea.com/wayhood/gophp/ext/standard"
)

func main() {
	p := phpctx.NewProcess("fpm")
	p.CommandLine(os.Args)

	l, err := net.Listen("unix", "/tmp/php-fpm.sock")
	if err != nil {
		log.Fatalf("failed to listne: %s", err)
	}

	err = fcgi.Serve(l, p.Handler("."))
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
