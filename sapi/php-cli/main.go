package main

import (
	"context"
	"log"
	"os"

	"gitea.com/wayhood/gophp/core/phpctx"
	_ "gitea.com/wayhood/gophp/ext/ctype"
	_ "gitea.com/wayhood/gophp/ext/date"
	_ "gitea.com/wayhood/gophp/ext/gmp"
	_ "gitea.com/wayhood/gophp/ext/hash"
	_ "gitea.com/wayhood/gophp/ext/json"
	_ "gitea.com/wayhood/gophp/ext/pcre"
	_ "gitea.com/wayhood/gophp/ext/standard"
)

func main() {
	p := phpctx.NewProcess("cli")
	p.CommandLine(os.Args)
	ctx := phpctx.NewGlobal(context.Background(), p)
	if len(os.Args) == 2 {
		if err := ctx.RunFile(os.Args[1]); err != nil {
			log.Printf("failed to run file: %s", err)
			os.Exit(1)
		}
	}
}
