package main

import (
	"log"
	"net"
	"net/http"
	"os"

	"gitea.com/wayhood/gophp/core/phpctx"
	_ "gitea.com/wayhood/gophp/ext/ctype"
	_ "gitea.com/wayhood/gophp/ext/date"
	_ "gitea.com/wayhood/gophp/ext/gmp"
	_ "gitea.com/wayhood/gophp/ext/hash"
	_ "gitea.com/wayhood/gophp/ext/json"
	_ "gitea.com/wayhood/gophp/ext/pcre"
	_ "gitea.com/wayhood/gophp/ext/standard"
)

func main() {
	p := phpctx.NewProcess("httpd")
	p.CommandLine(os.Args)

	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %s", err)
	}

	log.Printf("[php-httpd] Listening on %s", l.Addr())

	path := "."

	if len(os.Args) == 2 {
		path = os.Args[1]
	}

	err = http.Serve(l, p.Handler(path))
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
