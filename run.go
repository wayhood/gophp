package main

import (
	"context"
	"log"
	"os"
	"gitea.com/wayhood/gophp/core/phpctx"
	_ "gitea.com/wayhood/gophp/ext/bz2"
	_ "gitea.com/wayhood/gophp/ext/ctype"
	_ "gitea.com/wayhood/gophp/ext/date"
	_ "gitea.com/wayhood/gophp/ext/gmp"
	_ "gitea.com/wayhood/gophp/ext/hash"
	_ "gitea.com/wayhood/gophp/ext/json"
	_ "gitea.com/wayhood/gophp/ext/pcre"
	_ "gitea.com/wayhood/gophp/ext/standard"
)

func main() {
	// by default, run script test.php
	p := phpctx.NewProcess("cli")
	p.CommandLine(os.Args)
	ctx := phpctx.NewGlobal(context.Background(), p)
	if err := ctx.RunFile("test.php"); err != nil {
		log.Printf("failed to run test file: %s", err)
		os.Exit(1)
	}
}
