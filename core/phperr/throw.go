package phperr

import "gitea.com/wayhood/gophp/core/phpv"

type PhpThrow struct {
	Obj phpv.ZObject
}

func (e *PhpThrow) Error() string {
	return "Uncaught Exception: ..." //TODO
}
