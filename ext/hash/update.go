package hash

import (
	gohash "hash"

	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpobj"
	"gitea.com/wayhood/gophp/core/phpv"
)

//> func bool hash_update ( HashContext $context , string $data )
func fncHashUpdate(ctx phpv.Context, args []*phpv.ZVal) (*phpv.ZVal, error) {
	obj := &phpobj.ZObject{Class: HashContext}
	var data phpv.ZString

	_, err := core.Expand(ctx, args, &obj, &data)
	if err != nil {
		return nil, err
	}

	h := obj.GetOpaque(HashContext).(gohash.Hash)
	_, err = h.Write([]byte(data))
	if err != nil {
		return nil, err
	}

	return phpv.ZBool(true).ZVal(), nil
}
