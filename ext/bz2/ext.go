package bz2

import (
	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpctx"
	"gitea.com/wayhood/gophp/core/phpv"
)

// WARNING: This file is auto-generated. DO NOT EDIT

func init() {
	phpctx.RegisterExt(&phpctx.Ext{
		Name:    "bz2",
		Version: core.VERSION,
		Classes: []phpv.ZClass{},
		Functions: map[string]*phpctx.ExtFunction{
			"bzdecompress": &phpctx.ExtFunction{Func: fncBzDecompress, Args: []*phpctx.ExtFunctionArg{}},
		},
		Constants: map[phpv.ZString]phpv.Val{},
	})
}
