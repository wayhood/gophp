package gmp

import (
	"math/big"

	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpv"
)

//> func GMP gmp_neg ( GMP $a )
func gmpNeg(ctx phpv.Context, args []*phpv.ZVal) (*phpv.ZVal, error) {
	var a *phpv.ZVal

	_, err := core.Expand(ctx, args, &a)
	if err != nil {
		return nil, err
	}

	i, err := readInt(ctx, a)
	if err != nil {
		return nil, err
	}

	r := &big.Int{}
	r.Neg(i)

	return returnInt(ctx, r)
}
