package standard

import (
	"time"

	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpctx"
	"gitea.com/wayhood/gophp/core/phpv"
)

//> func bool set_time_limit ( int $seconds )
func fncSetTimeLimit(ctx phpv.Context, args []*phpv.ZVal) (*phpv.ZVal, error) {
	var d phpv.ZInt
	_, err := core.Expand(ctx, args, &d)
	if err != nil {
		return nil, err
	}

	ctx.Global().(*phpctx.Global).SetDeadline(time.Now().Add(time.Duration(d) * time.Second))
	return phpv.ZNULL.ZVal(), nil
}
