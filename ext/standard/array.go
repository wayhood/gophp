package standard

import (
	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpv"
)

//> func array array_merge ( array $array1 [, array $... ] )
func fncArrayMerge(ctx phpv.Context, args []*phpv.ZVal) (*phpv.ZVal, error) {
	var a *phpv.ZArray
	_, err := core.Expand(ctx, args, &a)
	if err != nil {
		return nil, err
	}
	a = a.Dup() // make sure we do a copy of array

	for i := 1; i < len(args); i++ {
		b, err := args[i].As(ctx, phpv.ZtArray)
		if err != nil {
			return nil, err
		}
		err = a.MergeTable(b.HashTable())
		if err != nil {
			return nil, err
		}
	}

	return a.ZVal(), nil
}

//> func array in_array ( array $array1 [, array $... ] )
func fncInArray(ctx phpv.Context, args []*phpv.ZVal) (*phpv.ZVal, error) {
	var a *phpv.ZVal
	var b *phpv.ZArray
	_, err := core.Expand(ctx, args, &a, &b)
	if err != nil {
		return nil, err
	}

	it := b.NewIterator()
	for ; it.Valid(ctx); it.Next(ctx) {
		v, err := it.Current(ctx)
		if err != nil {
			return nil, err
		}
		if (a.Value() == v.Value()) {
			return phpv.ZBool(true).ZVal(), nil
			break
		}
	}
	return phpv.ZBool(false).ZVal(), nil
}

//func valueCheck(a, b *phpv.ZVal) bool {
//	if{
//
//	}
//	switch(a.GetType()) {
//	case phpv.ZtBool:
//		a.Value().
//
//	}
//	//z.GetType() == phpv.ZtArray
//}