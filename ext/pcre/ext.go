package pcre

import (
	"gitea.com/wayhood/gophp/core"
	"gitea.com/wayhood/gophp/core/phpctx"
	"gitea.com/wayhood/gophp/core/phpv"
)

// WARNING: This file is auto-generated. DO NOT EDIT

func init() {
	phpctx.RegisterExt(&phpctx.Ext{
		Name:    "pcre",
		Version: core.VERSION,
		Classes: []phpv.ZClass{},
		Functions: map[string]*phpctx.ExtFunction{
			"preg_quote":   &phpctx.ExtFunction{Func: pregQuote, Args: []*phpctx.ExtFunctionArg{}},
			"preg_replace": &phpctx.ExtFunction{Func: pregReplace, Args: []*phpctx.ExtFunctionArg{}},
		},
		Constants: map[phpv.ZString]phpv.Val{},
	})
}
