package json

import (
	"gitea.com/wayhood/gophp/core/phpobj"
	"gitea.com/wayhood/gophp/core/phpv"
)

//> class JsonSerializable
var JsonSerializable = &phpobj.ZClass{
	Type: phpv.ZClassTypeInterface,
	Name: "JsonSerializable",
}
